//========================================================================
//
// pdfsigchk.cc
//
// 2017-11-12 Vladimir Zabara <wlady2001@gmail.com>
//
// Check PDF and return:
//  0 - OK (Certificate is Trusted, Signature is Valid)
//  1 - check error
//  2 - signature was not found
//  3 - signature is wrong
//  4 - certificate is wrong
//
//========================================================================

#include "config.h"
#include <poppler-config.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "parseargs.h"
#include "Object.h"
#include "Array.h"
#include "Page.h"
#include "PDFDoc.h"
#include "PDFDocFactory.h"
#include "Error.h"
#include "GlobalParams.h"
#include "SignatureInfo.h"

static GBool printVersion = gFalse;
static GBool printHelp = gFalse;
static GBool dontVerifyCert = gFalse;

static const ArgDesc argDesc[] = {
  {"-nocert", argFlag,     &dontVerifyCert,     0,
   "don't perform certificate validation"},
  {"-v",      argFlag,     &printVersion,  0,
   "print copyright and version info"},
  {"-h",      argFlag,     &printHelp,     0,
   "print usage information"},
  {"-help",   argFlag,     &printHelp,     0,
   "print usage information"},
  {"-?",      argFlag,     &printHelp,     0,
   "print usage information"},
  {NULL}
};

const char *strCodes = "\nReturn codes:\n\
\n\
 0 - OK \n\
 1 - check error \n\
 2 - signature was not found \n\
 3 - signature is wrong \n\
 4 - certificate is wrong \n\
\n\
Example - check signed pdf file but skip checking certificate:\n\
\n\
pdfsigchk -nocert signed.pdf\n\
echo $?\n\n";

int main(int argc, char *argv[])
{
  PDFDoc *doc = NULL;
  unsigned int sigCount;
  GooString * fileName = NULL;
  SignatureInfo *sig_info = NULL;
  std::vector<FormWidgetSignature*> sig_widgets;
  globalParams = new GlobalParams();

  int exitCode = 1;
  GBool ok;

  ok = parseArgs(argDesc, &argc, argv);

  if (!ok || argc != 2 || printVersion || printHelp) {
    fprintf(stderr, "pdfsigchk version %s\n", PACKAGE_VERSION);
    if (!printVersion) {
      printUsage("pdfsig", "<PDF-file>", argDesc);
      fwrite(strCodes, 1, strlen(strCodes), stdout);
    }
    exitCode = 1;
    goto end;
  }

  fileName = new GooString(argv[argc - 1]);

  // open PDF file
  doc = PDFDocFactory().createPDFDoc(*fileName, NULL, NULL);

  if (!doc->isOk()) {
    exitCode = 1;
    goto end;
  }

  sig_widgets = doc->getSignatureWidgets();
  sigCount = sig_widgets.size();

  if (sigCount == 0) {
    exitCode = 2;
    goto end;
  }

  for (unsigned int i = 0; i < sigCount; i++) {
    sig_info = sig_widgets.at(i)->validateSignature(!dontVerifyCert, false);
    if (sig_info->getSignatureValStatus() != SIGNATURE_VALID) {
      exitCode = 3;
      goto end;
    }
    if (!dontVerifyCert && sig_info->getCertificateValStatus() != CERTIFICATE_TRUSTED) {
      exitCode = 4;
      goto end;
    }
  }

  exitCode = 0;

end:
  delete globalParams;
  delete fileName;
  delete doc;

  return exitCode;
}
