//========================================================================
//
// pdfauth.cc
//
// 2017-11-12 Vladimir Zabara <wlady2001@gmail.com>
//
// Check PDF and return:
//  0 - OK (Certificate is Trusted, Signature is Valid, CSRF is Valid)
//  1 - check error
//  2 - signature was not found
//  3 - signature is wrong
//  4 - certificate is wrong
//  5 - CSRF token is wrong
//
// Note: CSRF token should be placed into Keywords field
//
//========================================================================

#include "config.h"
#include <poppler-config.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "parseargs.h"
#include "Object.h"
#include "Array.h"
#include "Dict.h"
#include "XRef.h"
#include "Catalog.h"
#include "Page.h"
#include "PDFDoc.h"
#include "PDFDocFactory.h"
#include "PDFDocEncoding.h"
#include "HtmlOutputDev.h"
#include "Error.h"
#include "GlobalParams.h"
#include "SignatureInfo.h"

static GooString* getInfoString(Dict *infoDict, const char *key);

static GBool printVersion = gFalse;
static GBool printHelp = gFalse;
static GBool dontVerifyCert = gFalse;
static char textEncName[128] = "";
static char ownerPassword[33] = "\001";
static char userPassword[33] = "\001";

static const ArgDesc argDesc[] = {
  {"-nocert", argFlag,     &dontVerifyCert,     0,
   "don't perform certificate validation"},
  {"-v",      argFlag,     &printVersion,  0,
   "print copyright and version info"},
  {"-h",      argFlag,     &printHelp,     0,
   "print usage information"},
  {"-help",   argFlag,     &printHelp,     0,
   "print usage information"},
  {"-?",      argFlag,     &printHelp,     0,
   "print usage information"},
  {NULL}
};

const char *strCodes = "\nReturn codes:\n\
\n\
 0 - OK \n\
 1 - check error \n\
 2 - signature was not found \n\
 3 - signature is wrong \n\
 4 - certificate is wrong \n\
 5 - CSRF token is wrong\n\
\n\
Example - check signed pdf file but skip checking certificate:\n\
\n\
pdfauth -nocert signed.pdf csrftoken\n\
echo $?\n\n";

int main(int argc, char *argv[])
{
  PDFDoc *doc = NULL;
  unsigned int sigCount;
  GooString *fileName = NULL, *csrfToken = NULL;
  GooString *ownerPW, *userPW, *subject;
  Object info;
  SignatureInfo *sig_info = NULL;
  std::vector<FormWidgetSignature*> sig_widgets;
  globalParams = new GlobalParams();

  int exitCode = 1;
  GBool ok;

  ok = parseArgs(argDesc, &argc, argv);

  if (!ok || argc != 3 || printVersion || printHelp) {
    fprintf(stderr, "pdfauth version %s\n", PACKAGE_VERSION);
    if (!printVersion) {
      printUsage("pdfauth", "<PDF-file> <CSRF-token>", argDesc);
      fwrite(strCodes, 1, strlen(strCodes), stdout);
    }
    exitCode = 1;
    goto end;
  }

  fileName = new GooString(argv[1]);
  csrfToken = new GooString(argv[2]);

  if (textEncName[0]) {
    globalParams->setTextEncoding(textEncName);
  }

  // open PDF file
  if (ownerPassword[0] != '\001') {
    ownerPW = new GooString(ownerPassword);
  } else {
    ownerPW = NULL;
  }
  if (userPassword[0] != '\001') {
    userPW = new GooString(userPassword);
  } else {
    userPW = NULL;
  }

  if (fileName->cmp("-") == 0) {
      delete fileName;
      fileName = new GooString("fd://0");
  }

  // open PDF file
  doc = PDFDocFactory().createPDFDoc(*fileName, ownerPW, userPW);

  if (userPW) {
    delete userPW;
  }
  if (ownerPW) {
    delete ownerPW;
  }
  if (!doc->isOk()) {
    exitCode = 1;
    goto end;
  }

  sig_widgets = doc->getSignatureWidgets();
  sigCount = sig_widgets.size();

  if (sigCount == 0) {
    exitCode = 2;
    goto end;
  }

  for (unsigned int i = 0; i < sigCount; i++) {
    sig_info = sig_widgets.at(i)->validateSignature(!dontVerifyCert, false);
    if (sig_info->getSignatureValStatus() != SIGNATURE_VALID) {
      exitCode = 3;
      goto end;
    }
    if (!dontVerifyCert && sig_info->getCertificateValStatus() != CERTIFICATE_TRUSTED) {
      exitCode = 4;
      goto end;
    }
  }

  doc->getDocInfo(&info);
  if (info.isDict()) {
    subject = getInfoString(info.getDict(), "Keywords");
  } else {
    exitCode = 1;
    goto end;
  }
  info.free();

  if (!subject->getLength()) {
    exitCode = 1;
    goto end;
  }

  if (csrfToken->cmp(subject) != 0) {
    exitCode = 5;
    goto end;
  }

  exitCode = 0;

end:
  delete globalParams;
  delete fileName;
  delete csrfToken;
  delete doc;

  return exitCode;
}

static GooString* getInfoString(Dict *infoDict, const char *key) {
  Object obj;
  GooString *s1, *s2;

  if (infoDict->lookup(key, &obj)->isString()) {
    s1 = obj.getString();
    s2 = new GooString(s1->getCString());
  } else {
    s2 = new GooString("");
  }
  obj.free();

  return s2;
}
