# Установка #

> Для правильной сборки необходим установленный пакет **nss-devel**.
> Процесс компиляции и установки необходимо выполнять с правами **root**.

Скачать исходные коды пакета **poppler** и распаковать. Скопировать **pdfauth.cc** в папку **~/poppler-xx.xx/utils/** и выполнить в корневой папке пакета

```sh
./configure
make
make install
```

> По-умолчанию утилиты будут установлены в **/usr/local/bin**.

В папке **~/poppler-xx.xx/utils/** отредактировать файл **Makefile** (можно скопировать необходимые строки из соответствующих настроек для **pdfinfo**) и запустить в папке **~/poppler-xx.xx/utils/**

```sh
make pdfauth
```

Скопировать полученный бинарный файл в **/usr/local/bin**

```sh
cp ~/poppler-xx.xx/utils/.libs/pdfauth /usr/local/bin/pdfauth
```

## pdfsigchk ##

Утилита проверки подписи PDF файла. В отличие от стандартной **pdfsig** не выводит текстовых сообщений и возвращает код проверки:

- 0 - OK (Certificate is Trusted, Signature is Valid)
- 1 - check error
- 2 - signature was not found
- 3 - signature is wrong
- 4 - certificate is wrong

## pdfauth ##

Утилита проверки подписи PDF файла и CSRF токена внутри этого файла (мета тэг Keywords). Является расширением **pdfsigchk**. Возвращаемые коды:

- 0 - OK (Certificate is Trusted, Signature is Valid, CSRF is Valid)
- 1 - check error
- 2 - signature was not found
- 3 - signature is wrong
- 4 - certificate is wrong
- 5 - CSRF token is wrong

## Генерация PDF файла с CSRF токеном ##

- выполнить подстановку реального значения CSRF Token вместо строки **csrf-token-placeholder** в файле **pdfmark**
- сгенерировать новый PDF файл при помощи **ghostscript**

```sh
gs -q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=ready.pdf empty.pdf pdfmark
```

### Обобщенная схема процесса аутентификации ###

![Обобщенная схема процесса аутентификации](pdf-csrf-auth.jpg)


## Другие проекты ##

Аналогичный скрипт на PHP для проверки подписи PDF файла, сохранения сертификатов и некоторых других функций можно найти в https://bitbucket.org/pdfsecutlis/pdfsec
